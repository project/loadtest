// Copied from update.js to handle the progress bar while running tests

if (Drupal.jsEnabled) {
  $(document).ready(function() {
      //$('#edit-has-js').each(function() { this.value = 1; });
    $('#progress').each(function () {
      var holder = this;

      // Success: redirect to the summary.
      var runtestsCallback = function (progress, status, pb) {
        if (progress == 100) {
          pb.stopMonitoring();
          window.location = window.location.href.split('/runtests')[0];
        }
      }

      // Failure: point out error message and provide link to the summary.
      var errorCallback = function (pb) {
        var div = document.createElement('p');
        div.className = 'error';
        $(div).html('An unrecoverable error has occured. You can find the error message below. It is advised to copy it to the clipboard for reference. Please continue to the <a href="update.php?op=error">update summary</a>');
        $(holder).prepend(div);
        $('#wait').hide();
      }

      var progress = new Drupal.progressBar('testprogress', runtestsCallback, "POST", errorCallback);
      progress.setProgress(-1, 'Running tests');
      $(holder).append(progress.element);
      progress.startMonitoring('running', 0);
    });
  });
}

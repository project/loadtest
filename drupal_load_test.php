<?php
class DrupalLoadTest extends DrupalTestCase {

  /**
   * Just some info for the reporter
   */
  function run(&$reporter) {
    $arr = array('class' => get_class($this));
    if (method_exists($this, 'get_info')) {
      $arr = array_merge($arr, $this->get_info());
    }
    $reporter->test_info_stack[] = $arr;
    //parent::run($reporter);
    $this->timed_run($reporter, $arr['class']);
    array_pop($reporter->test_info_stack);
  }

  /**
   * Adds timing information when running a test
   */
  function timed_run(&$reporter, $name) {
    $stime = loadtest_starttimer();
    parent::run($reporter);
    loadtest_stoptimer($stime, $reporter, $name);
  }


  function _get_config_val($var, &$form) {
    $retval = variable_get(get_class($this) . '_' . $var, NULL);
    if ($retval === NULL) {
      $retval = $form[$var]['#default_value'];
    }
    return $retval;
  }
  
  function config($var = NULL) {    
    if (!method_exists($this, 'config_form')) {
      return NULL;
    }
    $form = $this->config_form();
    if ($var) {
      $retval = $this->_get_config_val($var, $form);
    }
    else {
      $retval = array();
      foreach (array_keys($form) as $var) {
        $retval[] = $this->_get_config_val($var, $form);
      }
    }
    return $retval;
  }
}

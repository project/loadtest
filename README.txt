Description
-----------------------
A framework for running and building load tests on a Drupal site.

States
-----------------------
There are two core parts to this module. The first part of this module lets you define and configure "states" for your site that you can run tests against.

Each state defines a set of modules and variables that will be enabled. When running a test you may select one of these pre-configured states to test against. The 'set to state' link sets the site to that state.

States can also be set remotely. This is useful for scripting load tests against your site. As an example, you could have a script as follows:

----
#!/bin/bash

curl http://localhost/drupal/admin/build/loadtest/set_state/test_state > /dev/null
ab2 -c1 -n500 http://localhost/drupal/index.php > test1a.ab
ab2 -c1 -n500 http://localhost/drupal/index.php > test1b.ab

curl http://localhost/drupal/admin/build/loadtest/set_state/test_state_nocach... > /dev/null
ab2 -c1 -n500 http://localhost/drupal/index.php > test2a.ab
ab2 -c1 -n500 http://localhost/drupal/index.php > test2b.ab
----

This script will automatically change the states as it runs and uses apache's benchmarking tool to run the test. Note that this script doesn't authenticate to Drupal at all, and so in this case 'anonymous' is actually setting up states. Obviously this is a major security risk and thus should not be used on production sites. There is a switch to enable and disable the setting of states by non-admin users.

admin/build/loadtest/set_state/on - enable state setting
admin/build/loadtest/set_state/off - disable state setting


Load testing
-----------------------
The second part of the module lets an administrator run tests within the drupal site. The load tests can use preconfigured states. As a module developer, you can also write your own test suites and load tests that will be used when running the load test module. Writing load tests is almost exactly the same as writing simpletest tests (http://drupal.org/simpletest). Except instead of inheriting from DrupalTestCase, inherit from DrupalLoadTest. Also, be sure that the load test is in the 'Load testing' group. This adds timing and logging to your test. You can also setup configurable options for a load test. See the request_pages.test config_form() method as a reference.


Test suites
-----------------------
A test suite is basically just some logic defining how to run a set of tests. There are some default pre-written test suites.

1. Single run - Just runs a single test on the site, doesn't play with modules or settings.
2. Individual Modules - tests each module on it's own and then runs the same tests with all modules enabled.

To add your own, write a function that calls 'simpletest_run_tests' somewhere in it, and then use the loadtest_register_test_suite() function so that loadtest knows about your function. For example, the loadtest implements the "single run" test as follows:

----
function loadtest_init() {
  loadtest_register_test_suite("loadtest_test_single_run", "Single run");
}

function loadtest_test_single_run($tests_list) {
  simpletest_run_tests($tests_list);
}
----


Gotchya's
-----------------------
 * Make sure that apache's Timeout is set to a large enough value that the script won't time out while the tests are running. If you run into errors in the middle of a test run check this value first.

 * If a test fails for whatever reason while it's running, then the site will be left in a potentially non usable state. This is because some of the tests disable all of the active modules on the site. Also, the variable 'loadtest_test_status' will need to be removed from the 'variable' table.

 * The same USAGE WARNING from simpletest applies to this module as well.

Authors
-----------------------
Scott Hadfield < hadsie at gmail dot com >

Thanks
-----------------------
This project was done as a Google Summer of Code project, so thanks Google for sponsoring it. And thanks to my mentors Khalid Baheyeldin, Rok Zlender, and Owen Barton for all your help and ideas.
